// Requiring the HTTP module
let http = require("http");

// Creating the server
// createServer() method

http.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'});

	response.end('Hello Leonell Cruz')
}).listen(4000);

// Create a console log to indicate that the server is running.

console.log('Server running at localhost:4000');

// at git bash command line, run node index.js